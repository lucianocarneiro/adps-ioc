require stream
epicsEnvSet(EPICS_DRIVER_PATH, "$(PWD)/cellMods:$(EPICS_DRIVER_PATH)")
epicsEnvSet(PORT,"FASTPS_1")
require caenelsmagnetps

# FAST-PS unit
iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=FASTPS_1, IP_ADDR=172.30.4.189, P=LEBT:, R=PwrC-PSCH-01:, MAXVOL=20, MAXCUR=20, HICUR=17, HIHICUR=19")

# NGPS unit
#iocshLoad("$(caenelsmagnetps_DIR)/caenelsngps.iocsh", "Ch_name=NGPS_1, IP_ADDR=192.168.0.2, P=MEBT:, R=PwrC-Quad-001:, MAXVOL=20, MAXCUR=300, HICUR=290, HIHICUR=295")
#asynSetTraceIOMask("$(PORT)",0,3)
#asynSetTraceMask("$(PORT)",0,255)
iocInit()

